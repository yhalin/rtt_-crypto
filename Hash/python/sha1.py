import numpy as np
import hashlib

def create_item():
    first = np.random.randint(20,high = 0xff, size=1024)
    str = ""
    for item in first:
        str += hex(item).replace("0x","")
    source = []
    for by in first:
        source.append(by)
    bye = bytes(source)
    n = hashlib.sha1()
    n.update(bye)
    return str,n.hexdigest()

dat = "[L = 20]\n\n"
for i in range(10):
    msg,has = create_item()
    dat += "Len = 8192\nMsg = " + msg + "\nMD = " + has + "\n\n"
    print(has)
print(dat)

file = open("sha_test_vector","w")
file.write(dat)
file.close()
